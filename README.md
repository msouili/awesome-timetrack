# Awesome Timetrack

This is an Enterprise Solution, which allows employees to track their time and attendance in the company as well as to monitor their working hours and time taken on breaks.

----

## Setting up the project:

Before starting it is required that Docker and Docker-compose installed on your host machine.

### 1) Cloning the project
1. Navigate to the directory where you wish to clone the project, for example:
   /home/user/Projects/
2. Open your terminal and type in:

```sh
 > git clone https://gitlab.com/msouili/awesome-timetrack.git
```

### 2) Launching the docker containers
1. Go to the root directory of the project, for example:
   /home/user/Projects/awesome-timetrack
2. Open the terminal and type the following command:

```sh
 > docker-compose up -d
```

### 3) Installing the project dependencies
```sh
 > docker exec -it php-fpm-at composer install
```

And now navigate to http://localhost and you're ready to go.


## Additional commands

### Managing the database
```sh
 > docker exec -it mysql-at mysql -u timetrack -p
```

### Accesing the logs

```sh
 > docker-compose logs -f
```
